Revisited design of the rd53a hybrid. Link to the original design:

https://github.com/kdunne/rd53a_singlechip_flex

Changes:
- correct the traces for the digital shunt (pads 142,143,144)
- correct wirebonds
- add 3 lanes for HitOr
- remove unused signals (ext)
- add a shunt resistor for each chip R1
- connect MUX output
- connect Vctrl to Vddd
- correct the shunt layout for Vref and Ioffset
- add Vref hack: SLDO_VREFA to VDDA
- change the layout for the flex connector
